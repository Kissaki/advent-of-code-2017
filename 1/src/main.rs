mod common;
mod textchecksum;
mod table;
mod grid;
mod password;
mod day5_jumps;

use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    for arg in args[1..].iter() {
        run(arg.parse::<i32>().unwrap());
    }
    // textchecksum::execute();
    // table::execute();
    // grid::execute();
    // password::execute();

    // day5_jumps::execute();
}

fn run(day: i32) {
    match day {
        1 => textchecksum::execute(),
        2 => table::execute(),
        3 => grid::execute(),
        4 => password::execute(),
        5 => day5_jumps::execute(),
        _ => panic!("Invalid parameter {}", day),
    }
}