use std::collections::HashSet;
use common;

pub fn execute() {
    let mut valid = 0;
    let input = common::get_input("day4_password.txt");
    for line in input.lines() {
        if check_password_valid(line) {
            valid += 1;
        }
    }
    println!("Number passwords: {}", input.lines().count());
    println!("Number of valid passwords: {}", valid);
}

pub fn check_password_valid(password: &str) -> bool {
    let mut words = HashSet::new();
    for word in password.split_whitespace() {
        if words.contains(word) {
            return false;
        }
        words.insert(word);
    }
    true
}

#[cfg(test)]
mod test {
    #[test]
    fn check_password_valid() {
        let testdata = [
            ("aa bb cc dd ee", true),
            ("aa bb cc dd aa", false),
            ("aa bb cc dd aaa", true),
        ];
        for td in testdata.iter() {
            assert_eq!(super::check_password_valid(td.0), td.1);
        }
    }
}
