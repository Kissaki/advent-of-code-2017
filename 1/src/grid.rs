use std::collections::HashMap;
use std::cmp;

const POSITION_NUMBER: i32 = 347991;
const FIND_VALUE_BIGGER_THAN: i32 = 347991;

pub fn execute() {
    println!("Day3 part 1: position number {}, distance: {}", POSITION_NUMBER, distance(POSITION_NUMBER));
    println!("Day3 part 2: {}", find_first_value_larger_than(FIND_VALUE_BIGGER_THAN));
}

fn distance(position: i32) -> i32 {
    let (target, src) = position_to_coords(position);
    let (xt, yt) = target;
    let (xs, ys) = src;
    return (xt - xs).abs() + (yt - ys).abs();
}

fn position_to_coords(position_number: i32) -> ((i32, i32), (i32, i32)) {
    let target = (0, 0);
    let coord = position_to_coord(position_number);
    return (target, coord);
}

fn position_to_coord(position_number: i32) -> (i32, i32) {
    let mut moves = position_number - 1;
    let mut x = 0;
    let mut y = 0;
    let mut width = 1;
    let mut height = 1;

    while moves > 0 {
        // right
        for _ in 0..cmp::min(moves, width) {
            x += 1;
            moves -= 1;
        }
        width += 1;
        // up
        for _ in 0..cmp::min(moves, height) {
            y -= 1;
            moves -= 1;
        }
        height += 1;
        // left
        for _ in 0..cmp::min(moves, width) {
            x -= 1;
            moves -= 1;
        }
        width += 1;
        // down
        for _ in 0..cmp::min(moves, height) {
            y += 1;
            moves -= 1;
        }
        height += 1;
    }
    (x, y)
}

#[allow(dead_code)]
fn filling_value(position_number: i32) -> i32 {
    let mut moves = position_number - 1;
    let mut x = 0i32;
    let mut y = 0i32;
    let mut width = 1;
    let mut height = 1;
    let mut values: HashMap<(i32, i32), i32> = HashMap::new();
    values.insert((0, 0), 1);

    while moves > 0 {
        // right
        for _ in 0..cmp::min(moves, width) {
            x += 1;
            moves -= 1;
            let field_value = calc_field_value(&values, (x, y));
            values.insert((x, y), field_value);
        }
        width += 1;
        // up
        for _ in 0..cmp::min(moves, height) {
            y -= 1;
            moves -= 1;
            let field_value = calc_field_value(&values, (x, y));
            values.insert((x, y), field_value);
        }
        height += 1;
        // left
        for _ in 0..cmp::min(moves, width) {
            x -= 1;
            moves -= 1;
            let field_value = calc_field_value(&values, (x, y));
            values.insert((x, y), field_value);
        }
        width += 1;
        // down
        for _ in 0..cmp::min(moves, height) {
            y += 1;
            moves -= 1;
            let field_value = calc_field_value(&values, (x, y));
            values.insert((x, y), field_value);
        }
        height += 1;
    }
    let v = values.get(&(x, y)).unwrap().clone();
    v
}

fn calc_field_value(values: &HashMap<(i32, i32), i32>, coord: (i32, i32)) -> i32 {
    let (x, y) = coord;
    let mut field_value = 0;
    for xx in x-1..x+2 {
        for yy in y-1..y+2 {
            match values.get(&(xx, yy)) {
                Some(v) => field_value += v,
                None => {},
            }
        }
    }
    field_value
}

fn find_first_value_larger_than(find_value: i32) -> i32 {
    let mut x = 0i32;
    let mut y = 0i32;
    let mut width = 1;
    let mut height = 1;
    let mut values: HashMap<(i32, i32), i32> = HashMap::new();
    values.insert((0, 0), 1);

    loop {
        // right
        for _ in 0..width {
            x += 1;
            let field_value = calc_field_value(&values, (x, y));
            if field_value > find_value {
                return field_value;
            }
            values.insert((x, y), field_value);
        }
        width += 1;
        // up
        for _ in 0..height {
            y -= 1;
            let field_value = calc_field_value(&values, (x, y));
            if field_value > find_value {
                return field_value;
            }
            values.insert((x, y), field_value);
        }
        height += 1;
        // left
        for _ in 0..width {
            x -= 1;
            let field_value = calc_field_value(&values, (x, y));
            if field_value > find_value {
                return field_value;
            }
            values.insert((x, y), field_value);
        }
        width += 1;
        // down
        for _ in 0..height {
            y += 1;
            let field_value = calc_field_value(&values, (x, y));
            if field_value > find_value {
                return field_value;
            }
            values.insert((x, y), field_value);
        }
        height += 1;
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn distance_0() {
        assert_eq!(super::distance(1), 0);
    }
    
    #[test]
    fn distance_1() {
        assert_eq!(super::distance(2), 1);
        assert_eq!(super::distance(4), 1);
        assert_eq!(super::distance(6), 1);
        assert_eq!(super::distance(8), 1);
    }
    
    #[test]
    fn distance_2() {
        assert_eq!(super::distance(3), 2);
        assert_eq!(super::distance(5), 2);
        assert_eq!(super::distance(7), 2);
        assert_eq!(super::distance(9), 2);
        assert_eq!(super::distance(11), 2);
        assert_eq!(super::distance(15), 2);
        assert_eq!(super::distance(19), 2);
        assert_eq!(super::distance(23), 2);
    }
    
    #[test]
    fn distance_3() {
        assert_eq!(super::distance(10), 3);
        assert_eq!(super::distance(12), 3);
        assert_eq!(super::distance(14), 3);
        assert_eq!(super::distance(16), 3);
        assert_eq!(super::distance(18), 3);
        assert_eq!(super::distance(20), 3);
        assert_eq!(super::distance(22), 3);
        assert_eq!(super::distance(24), 3);
    }
    
    #[test]
    fn distance_4() {
        assert_eq!(super::distance(13), 4);
        assert_eq!(super::distance(17), 4);
        assert_eq!(super::distance(21), 4);
        assert_eq!(super::distance(25), 4);
    }
    
    #[test]
    fn distance_higher() {
        assert_eq!(super::distance(1024), 31);
    }

    #[test]
    fn distance_solution() {
        assert_eq!(super::distance(super::POSITION_NUMBER), 480);
    }

    #[test]
    fn filling_value() {
        assert_eq!(super::filling_value(1), 1);
        assert_eq!(super::filling_value(2), 1);
        assert_eq!(super::filling_value(3), 2);
        assert_eq!(super::filling_value(4), 4);
        assert_eq!(super::filling_value(5), 5);
    }

    #[test]
    fn find_first_value_larger_than() {
        assert_eq!(super::find_first_value_larger_than(1),2);
        assert_eq!(super::find_first_value_larger_than(2), 4);
        assert_eq!(super::find_first_value_larger_than(3), 4);
        assert_eq!(super::find_first_value_larger_than(4), 5);
        assert_eq!(super::find_first_value_larger_than(super::FIND_VALUE_BIGGER_THAN), 349975);
    }
}
