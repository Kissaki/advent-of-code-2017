use common;

pub fn execute() {
    let mut input = Vec::new();
    get_input(&mut input);
    let jumps = jump_count(&mut input);
    println!("Exit after {} jumps", jumps);
    
    let mut input2 = Vec::new();
    get_input(&mut input2);
    let jumps2 = jump2_count(&mut input2);
    println!("Exit after {} jumps2", jumps2);
}

fn get_input(input: &mut Vec<i32>)  {
    let content = common::get_input("day5_jumps.txt");
    for line in content.lines() {
        input.push(line.parse::<i32>().unwrap());
    }
}

// Returns the number of jumps until exit is reached.
fn jump_count(input: &mut Vec<i32>) -> i32 {
    let mut pos: i32 = 0;
    let mut jumps = 0;
    loop {
        if pos < 0 || pos >= input.len() as i32 {
            return jumps; 
        }
        let value = input[pos as usize];
        // Increment the position we will be leaving
        input[pos as usize] = value + 1;
        pos += value;
        jumps += 1;
    }
}

// Returns the number of jumps until exit is reached.
fn jump2_count(input: &mut Vec<i32>) -> i32 {
    let mut pos: i32 = 0;
    let mut jumps = 0;
    loop {
        if pos < 0 || pos >= input.len() as i32 {
            return jumps; 
        }
        let value = input[pos as usize];
        if value >= 3 {
            input[pos as usize] = value - 1;
        } else {
            input[pos as usize] = value + 1;
        }
        pos += value;
        jumps += 1;
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn jumps() {
        let mut input = vec![0, 3, 0, 1, -3];
        assert_eq!(super::jump_count(&mut input), 5);
        assert_eq!(input, vec![2, 5, 0, 1, -2]);
    }
    
    #[test]
    fn part1() {
        let mut input = Vec::new();
        super::get_input(&mut input);
        assert_eq!(super::jump_count(&mut input), 373160);
    }

    #[test]
    fn jumps2() {
        let mut input = vec![0, 3, 0, 1, -3];
        assert_eq!(super::jump2_count(&mut input), 10);
        assert_eq!(input, vec![2, 3, 2, 3, -1]);
    }
    
    #[test]
    fn part2_solution() {
        let mut input = Vec::new();
        super::get_input(&mut input);
        assert_eq!(super::jump2_count(&mut input), 26395586);
    }
}
