use common;

pub fn execute() {
    let input = common::get_input("day2_table.txt");
    println!("Day2 part 1 checksum: {}", day2_part1(&input));
    println!("Day2 part 1 checksum: {}", day2_part2(&input));
}

// Calculate checksum of spreadsheet
fn day2_part1(input: &str) -> i32 {
    let table_rows = common::table_to_ints(input);
    let mut checksum = 0;
    for row in &table_rows {
        let mut min = None;
        let mut max = None;
        for n in row.iter() {
            match min {
                Some(m) => {
                    if n < m {
                        min = Some(n);
                    }
                }
                None => min = Some(n),
            }
            match max {
                Some(m) => {
                    if n > m {
                        max = Some(n);
                    }
                }
                None => max = Some(n),
            }
        }
        let linesum = max.unwrap() - min.unwrap();
        checksum += linesum;
    }
    return checksum;
}

// Calculate checksum of spreadsheet
fn day2_part2(input: &str) -> i32 {
    let table_rows = common::table_to_ints(input);
    let mut checksum = 0;
    for row in &table_rows {
        let mut upper = None;
        let mut downer = None;
        for (i1, n1) in row.iter().enumerate() {
            for (i2, n2) in row.iter().enumerate() {
                if i1 != i2 && n1 % n2 == 0 {
                    match upper {
                        Some(_) => panic!("More than one clean division in row {:?}", row),
                        None => upper = Some(n1),
                    }
                    match downer {
                        Some(_) => panic!("More than one clean division in row {:?}", row),
                        None => downer = Some(n2),
                    }
                }
            }
        }
        let row_result = upper.unwrap() / downer.unwrap();
        checksum += row_result;
    }
    return checksum;
}

#[cfg(test)]
mod test {
    use common;

    #[test]
    fn day2_part1() {
        let testdata = [
            ("5 1 9 5
7 5 3
2 4 6 8", 18),
        ];
        for data in testdata.iter() {
            let (input, checksum) = *data;
            assert_eq!(super::day2_part1(input), checksum);
        }

        let input = common::get_input("day2_table.txt");
        assert_eq!(super::day2_part1(&input), 36174);
    }

    #[test]
    fn day2_part2() {
        let input = "5 9 2 8
9 4 7 3
3 8 6 5";
        let expected = 9;
        assert_eq!(super::day2_part2(input), expected);
    }
}
