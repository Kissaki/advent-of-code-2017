use std::fs::File;
use std::io::prelude::*;

// Returns the single content line from the file 'filename'.
pub fn get_input_line(filename: &str) -> String {
    let content = get_input(filename);
    assert!(content.lines().count() == 1);
    let line = String::from(content.lines().last().unwrap());
    line
}

// Returns the content of the file 'filename'.
// Users of this function will probably call lines() on the returned String,
// otherwise the get_input_line() function is probably preferred (single line).
pub fn get_input(filename: &str) -> String {
    let mut file = File::open(filename).unwrap();
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();
    content
}

pub fn table_to_ints(input: &str) -> Vec<Vec<i32>> {
    let mut lines = Vec::new();
    for line in input.lines() {
        let mut line_values = Vec::new();
        for value in line.split_whitespace() {
            line_values.push(value.parse().unwrap());
        }
        lines.push(line_values);
    }
    return lines;
}

pub fn string_to_ints(input: &str) -> Vec<i32> {
    let mut vec = Vec::with_capacity(input.chars().count());
    for c in input.chars() {
        vec.push(char_to_int(c));
    }
    return vec;
}

pub fn char_to_int(c: char) -> i32 {
    return c.to_string().parse().unwrap();
}

pub fn calc_sum(vec: Vec<i32>, offset: usize) -> i32 {
    let mut shifted = Vec::with_capacity(vec.len());
    for i in &vec[offset..] {
        shifted.push(i);
    }
    for i in &vec[..offset] {
        shifted.push(i);
    }

    let mut sum = 0;

    let mut other = shifted.iter();
    for value in vec.iter() {
        if &value == other.next().unwrap() {
            sum += value;
        }
    }

    return sum;
}
