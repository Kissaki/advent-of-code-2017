use common;

pub fn execute() {
    let input = common::get_input_line("day1_checksum.txt");
    println!("Day1 part 1 sum: {}", day1_part1(&input));
    println!("Day1 part 2 sum: {}", day1_part2(&input));
}

fn day1_part1(input: &str) -> i32 {
    let offset = 1;
    let vec = common::string_to_ints(input);
    return common::calc_sum(vec, offset);
}

fn day1_part2(input: &str) -> i32 {
    let chars_count = input.chars().count();
    if chars_count % 2 != 0 {
        panic!("Input is not an even number of elements.");
    }
    let offset = chars_count / 2;

    return common::calc_sum(common::string_to_ints(input), offset);
}

#[cfg(test)]
mod test {
    use common;

    #[test]
    fn day1_part1_test() {
        let testdata = [("1122", 3), ("1111", 4), ("1234", 0), ("91212129", 9)];

        for data in testdata.iter() {
            let (input, sum) = *data;
            assert_eq!(super::day1_part1(input), sum);
        }
        let input = common::get_input_line("day1_checksum.txt");
        assert_eq!(super::day1_part1(&input), 1102);
    }

    #[test]
    #[should_panic]
    fn day1_part2_uneven_input_test() {
        super::day1_part2("1");
    }

    #[test]
    fn day1_part2() {
        let testdata = [("1212", 6), ("1221", 0), ("123425", 4), ("123123", 12), ("12131415", 4)];

        for data in testdata.iter() {
            let (input, sum) = *data;
            assert_eq!(super::day1_part2(input), sum);
        }
        let input = common::get_input_line("day1_checksum.txt");
        assert_eq!(super::day1_part2(&input), 1076);
    }
}
